# Microservices World

1. Semantic Versioning

JAVA
1. Development
2. Release process (create git tag, pushing jar without source, and without test into Artifactory with tripple: group : artifact ID : version)
3. deployment of a single artifact (only applies to monoliths)


MICROSERVICES
1. Development
2. Release process (create git tag, pushing docker image without source, and without test into Docker Hub with tripple: group : artifact ID : version)
3. deployment of a complex group of components into multiple environments (local, dev, prod, pre-prod, demo, test)

| service | build | dev | test | demo | pre-prod | prod |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| traveler-service-fe                | latest | 2.2.0 | 2.2.0  | 2.1.9 | 2.1.8 | 2.1.8 |
| traveler-service-be           | latest | 4.6.4  | 4.6.1  |4.6.0 | 4.6.0 | 4.6.0 |
| upload-fe                | latest | 3.1.8  | 3.1.7  | 3.1.6 | 3.1.0 | 3.1.0 |
| upload-be  | latest | 2.1.0  | 2.1.0  | 2.1.0 | 2.1.0 | 2.1.0 |
| service-3           | latest | 2.1.0  | 2.1.0  |------ | ------ | ------ |
| service-4              | latest | 1.0.23 | 1.0.23 |------ | ------ | ------ |
| service-5              | latest | 2.1.0  | 2.1.0  |------ | ------ | ------ |
| service-6  | latest | 1.2.16 | 1.2.16 |------ | ------ | ------ |


1. freeze development should not be possible/needed
2. you are using snapshots


the problem is that the code relies on the environment which it will be run.

You cannot take a dev-branch or feature-branch or snapshot into the demo, pre-prod, prod env. You should only take RELEASEs and compose them into another SNAPSHOT or RELEASE



## NEXT STEPS:


1. slim down the source code repository:
* remove maven profiles
* remove application.properties
* remove application.yaml
* remove helm configuration
* remove docker configuration
* remove deployment logic currently hidden in branches
2. It should be possible to deploy a docker image that was produced without git, svn or any other VCS. The project should honor only SNAPSHOT/RELEASE convention, not a Version Control System internal features, such as branches, or git flow. The project should be fully compatible with software that was made without a Version Control System.
3. implement a gitlab pipeline git commit -> publish **SNAPSHOT**
4. implement a gitlab pipeline git tag -> publish **RELEASE**
5. deploy based on semantic versioning scheme not based on git branch model
6. The following artifactory repositories should be abandoned:
* digak-maven-demo
* digak-maven-dev
* digak-maven-preprod
* digak-maven-prod
* digak-maven-test
7. maven profiles must be moved into kubernetes configuration repositories and those configurations and passwords must be access restricted by gitLab ACL. Maven profiles were created because there was no Kubernetes. Since we already paid for Kubernetes, there is no reason to not take advantage of the framework.
8. Environment Variables, that are currently in Openshift, should be versioned. All factors that could affect the state of a deployment i.e. environment/namespace variables jvm properies, cli params should be stored in kubernetes configuration git repository, and they should be versioned just like software is. Putting global configuration in Openshift is not necessary.
9. Translations should be versioned.
10. Common Openshift settings should be extracted, reused and versioned.
11. GitLab pipelines should be versioned and reused by an import statement (not by copy-paste)
12. java jar, and docker image should be stripped of source code, and only binaries should be included
13. java jar should be stripped of test binaries
14. docker image should be stripped of JDK, and docker image should not be used for development or debugging
15. solely purpose of a docker image is to contain runtime environment, i.e. jar, jre (not source, not jdk, not maven)
16. docker images should be stripped of any configuration, they should be independent of the environment in which they will be deployed. The same docker image can be deployed in all 5 environments (dev, pre-prod, prod, demo, test).
17. the docker image and kubernetes configuration compose a unique key and they work in tandem (config + docker = deployment)
which means that there will be one docker image and 5 different configurations. **It is a one-to-many relation (one release = many deployments**)
18. The state of the Openshift namespace must be fully reversible, deployments must be reproducible, staleless, and versioned in git (including environment variables).
19. process should be fully automatic, and natural. No steps should be done manually, and no training should be required.
20. Developer job should be limited to git clone, git commit, git push (to guarantee a quick start)
21. Developer should be able to produce software independently from the deployments. For example. The developer can create and publish traveler-service version 111.1.1 while the team is still working on deploying a version 3.0.0 of the same module/microservice. (decouple development process from deployment process). In other words: work in parallel not sequentially.
22. work on your task in isolation. You don't need to checkout a source code that you are not going to work on. You will only checkout a small fragment of the large project, and use maven to download the rest of the project for you. This will also speed up start up time, and will make the development more efficient.
Btw. if the project grows significantly, imagine Netflix. How do you imagine to checkout the entire project on your laptop? If the project grows, you will hit the wall, where it will simply not start, because you will run out of RAM memory, and you will have no other choice than splitting it into smaller pieces, so you may as well predict the future, and start splitting it now. This is probably the most important point. If you want the project to be of enterprise class, you have no choice, because of physical limitations of your machine. You will not be able to start the server locally, when it needs more than 16GB of RAM.
23. all work must reversible, reproducible, and runnable on any platform.

## Summary
The process presented here is just a tool for the developers to make their work easier. All the processes should be automatized and abstracted from the end user. The goal is to adjust CI/CD process so that you, as a developer can work on smaller pieces and you can better focus on coding. The rest of the process, such as release, qa, deployment should be done by Continous Integration tools. The developers do not need to know how a piece of software ends up in the production. They are welcome to read about this, but as a user of the pipeline you are not required to know the implementation details. Only the DevOps team who created the pipeline needs to really understand the process. Once it is implemented you never need to go back to this. You will be an ordinary user of the git repository. The GitOps approach frees you from all those manual steps that were once used to release, test, deploy.
